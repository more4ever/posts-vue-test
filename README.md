# vue-test

<img src="https://i.ibb.co/SxbM5JH/Screenshot-2020-06-17-at-21-58-11.png" alt="Screenshot-2020-06-17-at-21-58-11" border="0" />

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
