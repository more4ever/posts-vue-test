import Vue from 'vue'
import App from './App.vue'
import vuexStore from './store';

import './styles/global.scss';


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store: vuexStore,
}).$mount('#app')
