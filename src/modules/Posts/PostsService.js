import BaseAxiosInstance from '../../libs/axios/BaseAxiosInstance';


const PostsService = {
  getList() {
    return BaseAxiosInstance.get('https://jsonplaceholder.typicode.com/posts');
  },
}

export default PostsService;
