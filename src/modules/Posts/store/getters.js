import { POSTS_LIST_GETTER } from './constants';


const postsGetters = {
  [POSTS_LIST_GETTER]: (state) => {
    return state.list;
  },
};

export default postsGetters;
