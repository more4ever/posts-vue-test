import postsActions from './actions';
import postsGetters from './getters';
import postsMutations from './mutations';


const postsStoreModule = {
  state: () => ({
    list: [],
  }),
  actions: postsActions,
  mutations: postsMutations,
  getters: postsGetters,
};

export default postsStoreModule;
