import { ADD_TOAST_MESSAGE } from 'vuex-toast'
import fallbackData from '../fallbackData.json'

import PostsService from '../PostsService';
import { POSTS_SYNC_LIST_ACTION, POSTS_UPDATE_LIST_MUTATION } from './constants';


const postsActions = {
  [POSTS_SYNC_LIST_ACTION]: async ({ commit, dispatch }) => {
    dispatch(
      ADD_TOAST_MESSAGE,
      {
        text: 'Data request has been started. Fallback data will be loaded if it failed',
        type: 'success',
      },
    );

    try {
      commit(POSTS_UPDATE_LIST_MUTATION, await PostsService.getList());
    } catch (error) {
      commit(POSTS_UPDATE_LIST_MUTATION, fallbackData);

      dispatch(
        ADD_TOAST_MESSAGE,
        {
          text: 'Request failed. fallback to hardcoded data',
          type: 'danger',
        },
      );
    }
  },
};

export default postsActions;
