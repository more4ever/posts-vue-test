import { POSTS_UPDATE_LIST_MUTATION } from './constants';


const postsMutations = {
  [POSTS_UPDATE_LIST_MUTATION]: (state, list) => {
    state.list = list;
  },
};

export default postsMutations;
