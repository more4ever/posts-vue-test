import Vue from 'vue';
import Vuex from 'vuex';
import { createModule } from 'vuex-toast'

import appConfig from './appConfig';
import postsStoreModule from './modules/Posts/store';


Vue.use(Vuex);

const vuexStore = new Vuex.Store({
  strict: appConfig.isDevelopment,
  modules: {
    posts: postsStoreModule,
    toast: createModule({
      dismissInterval: 8000,
    }),
  },
})

export default vuexStore;
