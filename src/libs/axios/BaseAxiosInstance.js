import axios from 'axios';


const BaseAxiosInstance = axios.create();

BaseAxiosInstance.interceptors.response.use(
  ({ data }) => data,
);

export default BaseAxiosInstance;
