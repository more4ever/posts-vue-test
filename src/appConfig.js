const environment = process.env.NODE_ENV;

const appConfig = {
  isDevelopment: environment !== 'production',
};

export default appConfig;
